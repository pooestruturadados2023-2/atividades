'''Class that stores dates, ensuring that those are valid'''
class Data:
    '''Stores, day, month and year'''
    def __init__(self, dia:int, mes:int, ano:int):
        self.__ano=self.ano=ano
        self.__mes=self.mes=mes
        self.__dia=self.dia=dia

    @property
    def dia(self):
        """Retorna o dia"""
        return self.__dia

    @property
    def mes(self):
        '''Retorna o mês'''
        return self.__mes

    @property
    def ano(self):
        '''Retorna o ano'''
        return self.__ano

    @dia.setter
    def dia(self,ndia):
        if self.check_num(ndia) is not True:
            print('Data inválida!')
        else:
            ndia=int(ndia)
            if ndia < 1: #impede dias negativos
                print(f'Dia inválido ({ndia}) - Dia Alterado para 1')
                ndia = 1
            if self.__mes == 2:
                if self.check_bis(self.__ano) is True:
                    if ndia <= 29:
                        self.__dia=ndia
                    else:
                        print(f'Data inválida ({ndia}/{self.mes}/{self.ano}) - Dia setado para 29!')
                        self.__dia=29
                else:
                    if ndia <= 28:
                        self.__dia=ndia
                    else:
                        print(f'Data inválida ({ndia}/{self.mes}/{self.ano}) - Dia setado para 28!')
                        self.__dia=28          
            elif self.__mes in (2,4,6,9,11):
                if ndia <= 30:
                    self.__dia=ndia
                else:
                    print(f'Data inválida ({ndia}/{self.mes}/{self.ano}) - Dia setado para 30!')
                    self.__dia=30
            else:
                if ndia <=31:
                    self.__dia=ndia
                else:
                    print(f'Data inválida ({ndia}/{self.mes}/{self.ano}) - Dia setado para 31!')
                    self.__dia=31

    @mes.setter
    def mes(self,nmes):
        if self.check_num(nmes) is True:
            nmes=int(nmes)
            if nmes > 12:
                print(f'Data inválida (mês = {nmes}) - Mês setado para 12!')
                nmes=12
            elif nmes < 1:
                print(f'Data inválida (mês = {nmes}) Mês setado para 1!')
                nmes=1
            self.__mes=nmes
        else:
            print('Data inválida!')

    @ano.setter
    def ano(self, nano):
        if self.check_num(nano) is True:
            self.__ano=nano
        else: print('Data inválida!')

    @staticmethod
    def check_num(numero):
        '''Check if the informed value is a number.'''
        return str(str(numero).replace('-','')).isnumeric()

    @staticmethod
    def check_bis(ano):
        '''Check if the informed year is a leap year.'''
        if (ano % 4 == 0 and ano % 100 != 0) or ano % 400 == 0:
            return True
        else:
            return False

    def __str__(self):
        return f'{self.dia}/{self.mes}/{self.ano}'


if __name__ == '__main__':
    data=Data(1,12,2003) #testando data sem erro
    print('Data 1:', data)
    data2=Data(7,65,2003) #testando data com erro no mês (acima de 12)
    print('Data 2:', data2)
    data3=Data(32,12,2000) #testando data com erro no dia (acima de 31 em um mês de 31 dias)
    print('Data 3:', data3)
    data4=Data(31,11,2000) #testando data com erro no dia (acima de 30 em um mês de 30 dias)
    print('Data 4:', data4)
    data5=Data(29,2,2001) #testando data com erro no dia (acima de 28/02 num ano não bissexto)
    print('Data 5:', data5)
    data6=Data(29,2,2000) #testando dia 29/02 num ano bissexto
    print('Data 6:', data6)
    data6.dia=30 #tentando alterar dia pata 30/02
    print('Data 6:', data6)
    data7=Data('10','20','2011') #passando com strings
    print('Data 7:', data7)
