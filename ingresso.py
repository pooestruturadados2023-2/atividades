from random import randint
'''randint para criar ingressos aleatórios'''

class Ingresso:
  def __init__(self, valor):
    self.valor=valor
  
  def imprimir_valor(self):
    return f'R${float(self.valor):.2f}'

class IngressoVIP(Ingresso):
  def __init__(self,valor,adicional):
    super().__init__(valor+adicional)

if __name__ == '__main__':

  #AUTOMATIZANDO
  ingressos=[]
  for i in range(3):
    ingressos.append(Ingresso(20))
    print(ingressos[i].valor)
    ingressos.append(IngressoVIP(ingressos[i].valor,2))
    print(ingressos[i].valor)
  
  num_ingresso=1
  for pos,ingresso in enumerate(ingressos):
    if isinstance(ingresso,IngressoVIP):
      print(f'Ingresso {num_ingresso} VIP: {ingresso.imprimir_valor()}',end='')
      print(f' Diferença: R${ingresso.valor-ingressos[pos-1].valor}')
      num_ingresso+=1
    else: print(f'Ingresso {num_ingresso}: {ingresso.imprimir_valor()}',end=', ')

  #print(', '.join([ingresso.imprimir_valor() for ingresso in ingressos]))
  
  #UTILIZANDO O HARD-CODING
  #i1=Ingresso(10)
  #i1vip=IngressoVIP(i1.valor,3)
  #i2=Ingresso(15)
  #i2vip=IngressoVIP(i2.valor,9)
  #i3=Ingresso(20)
  #i3vip=IngressoVIP(i3.valor,10)
  #print(i1.imprimir_valor())
  #print(i1vip.imprimir_valor())

#Crie uma classe chamada Ingresso, que possui um valor em reais e um método imprimirValor(). Crie uma classe IngressoVIP, que herda de Ingresso e possui um valor adicional. Crie um método que retorne o valor do ingresso VIP (com o adicional incluído). Crie um programa para criar as instâncias de Ingresso e IngressoVIP, mostrando a diferença de preços.