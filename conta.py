from os import system
from platform import platform

class ContaCorrente:
    numconta=1
    def __init__(self, titular, saldo):       
        self.__titular=titular
        self.__saldo=saldo
        self.__numero=self.__class__.numconta
        self.__class__.numconta+=1

    @property
    def numero(self):
        return self.__numero
    
    @property
    def saldo(self):
        return self.__saldo
    
    @property
    def titular(self):
        return self.__titular
    
    @saldo.setter
    def saldo(self,valor):
        print('Valor não pode ser alterado!')

    def depositar(self, valor):
        self.__saldo+=valor

    def sacar(self, valor):
        if self.__saldo >= valor:
            self.__saldo-=valor
            return True
        else:
            return False

    def __str__(self):
        return f'Conta {self.__numero} - {self.__titular}'

class Utilidades:

    @staticmethod
    def clear():
        '''
        Função para limpar a tela compatível com windows
        '''
        if 'windows' in str(platform()).lower():
            system('cls')
        else:
            system('clear')

    @staticmethod
    def lista_contas(lista:list):
        '''
        Função para listar as contas
        '''
        for i in lista:
            print(i)
        print('')

if __name__ == '__main__':
    Utilidades.clear()
    contas=[]
    for i in range(3):
        contas.append(ContaCorrente(input(f'Digite o nome do titular da conta {i+1}... '),float(input('Digite o valor da conta... '))))
        Utilidades.clear()
    while True:
        print('''\
Selecione uma opção:
[1] - Depositar
[2] - Sacar
[3] - Saldo
[4] - Sair
''')
        escolha=input('Digite a opção escolhida... ')
        if escolha not in ('1','2','3','4'):
            Utilidades.clear()
            print('Opção inválida!\n')
        elif escolha == '4':
            break
        elif escolha == '1':
            Utilidades.clear()
            Utilidades.lista_contas(contas)
            conta=input('Digite o numero da conta que você deseja realizar o deposito... ')
            valor=input('Digite o valor a ser depositado: ')
            if str(valor.replace('.','')).isnumeric() is False or valor.isnumeric() is False:
                Utilidades.clear()
                print('Valor inválido!\n')
            else:
                for i in contas:
                    if conta == str(i.numero):
                        i.depositar(float(valor))
                        valor=''
                        break
                Utilidades.clear()
                if valor != '':
                    print('Número da conta não encontrado!\n')
                else:
                    print('Depósito realizado com sucesso!\n')
        elif escolha == '2':
            Utilidades.clear()
            Utilidades.lista_contas(contas)
            conta=input('Digite o numero da conta que você deseja realizar o saque... ')
            valor=input('Digite o valor a ser sacado: ')
            if str(valor.replace('.','')).isnumeric() is False or valor.isnumeric() is False:
                Utilidades.clear()
                print('Valor inválido!\n')
            else:
                for i in contas:
                    if conta == str(i.numero):
                        sac_aux=i.sacar(float(valor))
                        valor=''
                        break
                Utilidades.clear()
                if valor != '':
                    print('Número da conta não encontrado\n!')
                elif sac_aux == True:
                    Utilidades.clear()
                    print('Saque realizado com sucesso!\n')
                elif sac_aux == False:
                    Utilidades.clear()
                    print('Saque não realizado! Saldo insuficiente!\n')
        else:
            Utilidades.clear()
            Utilidades.lista_contas(contas)
            conta=input('Digite o numero da conta que você deseja visualizar o saldo... ')
            valor=None
            for i in contas:
                if conta == str(i.numero):
                    Utilidades.clear()
                    print(f'Saldo atual: R${"%.2f" % i.saldo}\n')
                    valor=''
                    break
            if valor != '':
                Utilidades.clear()
                print('Número da conta não encontrado!\n')
