class Pais:
    def __init__(self,nome:str, capital:str, dimensao:float):
        self.__nome=nome
        self.__capital=capital
        self.__dimensao=dimensao
        self.fronteiras=[]
    
    @property
    def nome(self):
        return self.__nome
    
    @property
    def capital(self):
        return self.__capital
    
    @property
    def dimensao(self):
        return self.__dimensao
    
    def lista_fronteiras(self):
        if self.fronteiras != []:
            front=''
            for i in self.fronteiras:
                if i != self.fronteiras[len(self.fronteiras)-1]:
                    front+=i+', '
                else:
                    front+=i+'.'
            return front
        else:
            print('Não faz fronteira com nenhum país')

    def adiciona_pais(self, pais):
        '''
        Checa se o país não está presente na lista de países
        '''
        if pais not in self.fronteiras:
            self.fronteiras.append(pais)
        else:
            print('Pais já se encontra na lista de fronteiras')
    
    def __str__(self):
        return f'''
Pais: {self.nome}
Capital: {self.capital}
Dimenção: {round(self.dimensao,2)} Km2
Fronteiras: {self.lista_fronteiras()}
'''

if __name__ == '__main__':
    #testando a classe
    pais=Pais('Brasil','Brasília',8514876.00)
    pais.adiciona_pais('Uruguai')
    pais.adiciona_pais('Argentina')
    pais.adiciona_pais('Paraguai')
    pais.adiciona_pais('Bolívia')
    pais.adiciona_pais('Peru')
    pais.adiciona_pais('Colômbia')
    pais.adiciona_pais('Venezuela')
    pais.adiciona_pais('Guiana')
    pais.adiciona_pais('Suriname')
    
    #testando método __str__
    print(pais)

    #checando se é possível adicionar um país que já está presente à lista
    pais.adiciona_pais('Suriname')

    #testando método de listar fronteiras
    print(pais.lista_fronteiras())

